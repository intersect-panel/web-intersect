<?php
class indexController extends Controller{

    function indexAction(){
        if($this->getUser()) return $this->redirect('user/account');
        return $this->render('index');
    }

    function loginAction(){
        if(Router::websiteValue('', 'recaptcha_key_private') != '')
            if(Check::captcha(Router::get('g-recaptcha-response', ''))['success'] !== true){
                App::addAlert('error', "Error recaptcha");
                return $this->redirect('index');
            }

        $_SESSION['server'] = Router::post('server');
        App::initDB($_SESSION['server']);

        $Id = User::login(Router::post('account'), hash('sha256', Router::post('password')));
        if($Id != -1){
            $_SESSION['Id'] = $Id;
            return $this->redirect('user/account');
        }
        App::addAlert('error', "Error Identification");
        return $this->redirect('index');
    }

    function languageAction(){
        $_SESSION['lang'] = Router::get('key', 'en-us');
        return $this->redirect(Router::get('page', 'index'));
    }
}