<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 30/03/2019
 * Time: 14:09
 */

class apiController extends Controller
{
    function indexAction()
    {
        //TODO create json with option
        echo 'Index API';
        return 1;
    }

    public function loginAction(){
        $result = [];
        $result['Id'] = -1;
        header('Content-Type: application/json');
        if(Router::post('account', null) && Router::post('password', null) && Router::post('db', 0) !== null){
            Database::clean();
            App::initDB(Router::post('db', null));
            $Id = User::login(Router::post('account'),  Router::post('password'));
            if($Id != -1){
                $result['error'] = false;
                $result['Id'] = $Id;
                if(Router::post('userInfo', false))$result['account'] = User::Id($Id, User::class, 'users');
            }else{
                $result['error']  = true;
            }
        }else{
            $result['error']  = true;
        }
        echo json_encode($result);
        return 1;
    }

    public function onlineAction(){ //TODO REWORK
        $result = ['server' => Router::post('db', null)];
        if($result['server'] == null)
            $server = Server::intersect();
        else
            $server = Router::getConfigJSON(array(), 'servers', $result['server']);

        $result['serverName'] = $server['name'];
        $result['serverIp'] = gethostbyname($server['intersect_ip']);
        $result['serverPort'] = $server['config_server']['ServerPort'];

        if($server['config_server']['UseApi'])
            $APIw = $this->checkTCP($server['intersect_ip'], $server['config_server']['ApiPort']);
        else
            $APIw = false;

        $result['API'] = ['work' => $APIw];
        $number = 0;
        if($server['config_server']['ServerPort']){
            //shell_exec('netstat -a -o -n -p UDP | findstr /i '.$result['serverPort'])
            $number = file_get_contents('https://www.ascensiongamedev.com/resources/status.php?host='.$result['serverIp'].'&port='.$result['serverPort']);
            $SPW =  $number != '-1';
        }
        else
            $SPW = false;
        $result['ServerIntersect'] = ['work' => $SPW, 'number'=>$number];


        $pl = $server['config_server']['PlayerDatabase'];
        $link = $pl['Server'];
        if(strtolower($pl['Type'])=='sqlite')
            $link = $server['intersect_folder_server']."/resources/playerdata.db";
        $result['Database Player'] = $this->checkDB($link, $pl['Port'], $pl['Type'], $pl['Username'], $pl['Password'], $pl['Database']);

        $ga = $server['config_server']['GameDatabase'];
        $link = $ga['Server'];
        if(strtolower($ga['Type'])=='sqlite')
            $link = $server['intersect_folder_server']."/resources/gamedata.db";

        $result['Database Game'] = $this->checkDB($link, $ga['Port'], $ga['Type'], $ga['Username'], $ga['Password'], $ga['Database']);

        header('Content-Type: application/json');
        echo json_encode($result);
        return 1;
    }

    private function checkTCP($ip, $port){
        $fp = fsockopen($ip, $port, $no, $str, 5);
        return $fp !== false;
    }

    private function checkDB($ip, $port, $type, $user, $password, $database){
        try {
            if (strtolower( $type ) == 'sqlite') {
                $p = new PDO('sqlite:'.$ip);
            } else {
                $p = new PDO('mysql:host='.$ip.':'.$port.';dbname='.$database.';charset=utf8', $user, $password);
            }
            unset($p);
        }catch (Exception $e){return ['work'=>false, 'message'=>$e->getMessage()];}
        return ['work'=>true];
    }

}