# API
- api/login
    - account (Email or pseudo)
    - password (hash 256)
    - Option :
        - db (specific ID need to load database) => default : 0
        - userInfo (get all information for this account) => default : false
    - Return : Id and all specification for this account
    
- api/online
    - Option : db (specific ID need to load database) => default : 0
    - Return : API/Intersect Server/DB Player and DB Game is work or not
    