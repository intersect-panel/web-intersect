# Root

````json
{
  "servers": "...",
  "website": "..."
}
````
The basic json contains 2 categories: 
- One part: server
- Another part: website

## Servers
For servers there are 2 solutions. Auto mode or manual mode.
### Auto Mode
````json
{
  "0":{
    "name": "Game Name Intersect",
    "intersect_folder_server": "/path/to/directory/server",
    "intersect_ip": "example.com",
    "ranking": "..",
    "playerVariables": "..."
  }
}
````
"0" is unique ID of your server. You can add lot of servers (2, 20, 200, ...) no limit

|Name|Description
|---|---
|name|That's the name of your game
|intersect_folder_server|Is the location from the root of the root server of Intersect
|intersect_ip|The address of the server on the internet
|ranking|See section "[ranking](#ranking)"
|playerVariables|See section "[player variables](#pvariable)"

### Manual Mode
````json
{
 "id_0": {
   "name": "Intersect Server Remote",
   "intersect_folder_server": "",
   "intersect_ip": "",
   "config_server":{
     "ServerPort": 5400,
     "UseApi": false,
     "ApiPort": 5400,
     "PlayerDatabase": {
       "Type": "mysql",
       "Server": "localhost",
       "Port": 3306,
       "Database": "Intersect",
       "Username": "root",
       "Password": "root"
     },
     "GameDatabase": {
       "Type": "sqlite",
       "Server": "",
       "Port": 3306,
       "Database": "",
       "Username": "",
       "Password": ""
     }
   }
 }
}
````
"id_0" is unique ID of your server. You can add lot of servers. You don't have limit

|Name|Description
|---|---
|name|That's the name of your game
|intersect_folder_server|IMPORTANT: nothing = define manual mode !
|intersect_ip|The address of the server on the internet
|ranking|See section "[ranking](#ranking)"
|playerVariables|See section "[player variables](#pvariable)"
|config_server|Need all information to connect panel to server Intersect

#### config_server:
|Name|Description|Default|Possible Value
|---|---|---|---
|ServerPort|Number port of server Intersect|5400|[1 - 65535]
|UseApi|Run API is enable|false|true / false
|ApiPort|If API is enable, use this number|5400|[1 - 65535]
|PlayerDatabase|It's configuration Database Player for server
|GameDatabase|It's configuration Database Game for server

##### PlayerDatabase or GameDatabase (same options)
|name|Description|Default|Possible value
|---|---|---|---
|Type|Type|sqlite|sqlite / mysql
|Server|Ip server<br>Empty if type equals sqlite|localhost|xxx.xxx.xxx.xxx / Domain Name
|Port|Port server<br>Empty if type equals sqlite|3306|null or [1 - 65535]
|Database|Name database<br>Empty if type equals sqlite|
|Username|Username server
|Password|Password of username

#### <a name="ranking">ranking</a> 
The system allows you to mix the character's columns and all the variables related to him/her.
````json
{
 "ranking": {
  "0": {
   "type": "level"
  },
  "1": {
    "type": "variable",
    "id": 1
  }
 }
}
````
IMPORTANT key => "0" or "1" : If one of the two values is missing then a problem will be raised 

|Name|Description|Possible Value
|---|---|---
|type|Important|level, xp, gender, variable
|id|If you choice type : "variable", you need specific 'TextID' like \pv{x}<br>In editor, you can choice this number|0 - ...

#### <a name="pvariable">player variables</a> 
This system converts a variable defined in the editor into a value that can be understood in the player's panel. You can define if it is visible or not (by default any variable is invisible). You can also define for each possible value a translation for the player

````json
{
  "playerVariables": {
    "1": {
      "name": "Work primary",
      "hidden": false,
      "values": {
        "0": "Nothing",
        "1": "Farmer",
        "2": "Minor",
        "3": "Fisherman",
        "4": "Lumberjack"
      }
    },
    "2": {
      "name": "Experience of work primary",
      "hidden": false
    }
  }
}
````
"1", "2" is ID Text like /pv{x}. The "x" is ID

|Name|Description
|---|---
|name|Name of this variable
|hidden|If is visible in the panel. Default : true
|values|It's array with:<br> "value": "name for this value"

## Website
````json
{
  "website":{
    "title": "Panel Intersect Server",
    "copyright": "Copyright for <a href='https://www.freemmorpgmaker.com/' target='_blank'>Intersect Engine</a> & <a href='https://www.ascensiongamedev.com/' target='_blank'>Ascension Game Dev</a> | Propriety owner <a href='https://www.ascensiongamedev.com/profile/1-jcsnider/' target='_blank'>Jcsnider</a> & <a href='https://www.ascensiongamedev.com/profile/2-kibbelz/' target='_blank'>Kibbelz</a> | Coded & Designed by <a href='https://nichos.fr/' target='_blank'>Nichos</a>",
    "path": "/",
    "display_date": "Y-n-j",
    "recaptcha_key_site":  "",
    "recaptcha_key_secret": "",
    "refresh_cache": 7200,
    "debug": false,
    "language": "en-us",
    "composer": {
      "linfo": false
    }
  }
}
````
|Name|Description
|---|---
|title|Title displayed in the top left corner of the site and name of website
|copyright|Important to thank the creators
|path|The path after the domain name. Example : <br> https://example.com/panel<br>The path is "/panel"
|display_date|Corresponds to the date displayed on the site<br>Please refer to the site: [https://www.php.net/manual/en/function.date.php](https://www.php.net/manual/en/function.date.php)
|recaptcha_key_site|If you want to use google recaptcha
|recaptcha_key_secret|Refer : [https://www.google.com/recaptcha/intro/v3.html](https://www.google.com/recaptcha/intro/v3.html)
|refresh_cache|Time to second to refresh file cache
|debug|Disable cache (true) or not (false)
|language|Set default language (en-us). You have list in folder "language"
|composer|It's array to set different module.<br>linfo is used for display information server (VPS, Dedicated)
