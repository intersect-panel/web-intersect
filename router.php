<?php
session_start();
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;

require_once 'require.php';

$page = "index";
if(!empty(App::path($_SERVER['REQUEST_URI']))){
    $page = App::path($_SERVER['REQUEST_URI']);
    if($page == "/") $page = "index";
}
Router::initLink($page);
if(!App::bindFunc())
    var_dump("500 Error Server: ".Router::link());

