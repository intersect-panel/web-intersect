# Features
- Management account and character
    - Change password and email
    - Can see all characters
- Panel administration with:
    - List maps
    - List account
    - List bans
    - List Item
    - Edit config.json
    - List npcs
- Character can see:
    - Inventory (with equipped or not)
    - Quests (with each step)
    - Variables (if visible)
    - Location in map
    - Ranking
- API
- Multi language

# Documentation
- [API](/Documentation/API/api.md)
- [config.json](/Documentation/Config/config.json.md)
  
# Todo List

- [V0.9] Event
- [V0.8/9] Server Management
- [V0.8] Easy installer
- [V0.8] Create Buffer in class Database
- [V0.8] Languages support
- [V0.7] All Npc
- [V0.7] System Ranking
- [V0.6] Variable for each character
- [V0.6] Recaptcha


#### Not yet
 
- [V0.10] Encyclopedia
- [V0.10] API get all events
- [V0.11] Shop and Craft
- [V0.?] Detail all information (NPC, Event, ...)
- [V0.?] Administration edit and delete any information
- [V0.?] Design responsive