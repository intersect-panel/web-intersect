<?php

function incl($path){
    foreach (glob($path.'/*') as $filename)
        if (is_dir( $filename ))
            incl( $filename );
        else if (preg_match( '/.*\.php$/', $filename ))
            require_once __DIR__ . '/' . $filename;

}

require_once __DIR__.'/class/IController.php';
require_once __DIR__.'/class/controller.php';
require_once __DIR__.'/class/Itable.php';
require_once __DIR__.'/class/Table.php';

if(file_exists(__DIR__.'/vendor/autoload.php'))
    require_once __DIR__.'/vendor/autoload.php';

incl('class');
incl('controller');

Router::init(__DIR__);
App::init();
Language::init();

if(!empty($_SESSION['Id']))
    App::addVar('account', User::Id($_SESSION['Id'], User::class, 'users'));

