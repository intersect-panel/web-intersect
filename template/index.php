<div class="vertical-text">
    <div class="text-center">
        <div class="panel login">
            <div class="title black">
                <?= Language::getWord('Login', 'index', 'index', 'name')?>
            </div>
            <div class="description white">
                <form class="pure-form pure-form-stacked" method="POST" action="<?= App::asset('login') ?>">
                    <fieldset>
                        <label for="account"><?= Language::getWord('Account/Email', 'index', 'index', 'user')?></label>
                        <input id="account" type="text" placeholder="<?= Language::getWord('Account/Email', 'index', 'index', 'user')?>" name="account">

                        <label for="password"><?= Language::getWord('Password', 'index', 'index', 'password')?></label>
                        <input id="password" type="password" placeholder="<?= Language::getWord('Password', 'index', 'index', 'password')?>" name="password">

                        <label for="server"><?= Language::getWord('Server', 'index', 'index', 'server')?></label>
                        <select id="server" name="server">
                            <?php
                                foreach(Server::getServerEnable() as $value) {
                                    ?>
                                    <option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                                    <?php
                                }
                            ?>
                        </select>
                        <?php
                        if(($captcha = Router::websiteValue('', 'recaptcha_key_site')) != ''){
                            echo '<div class="g-recaptcha" data-sitekey="'.$captcha.'"></div>';
                        }
                        ?>
                        <input type="submit" class="pure-button button-success" value="<?= Language::getWord('Login', 'index', 'index', 'login')?>">
                    </fieldset>
                </form>
            </div>
            <div class="footer grey">
                SOON...
            </div>
        </div>
    </div>
</div>