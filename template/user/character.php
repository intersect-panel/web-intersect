<?php
/** @var User $account */
$account = App::getVar('account');
/** @var Character $character */
$character = App::getVar('character');
?>
<div class="pure-g marge">
    <div class="pure-u-1-6"></div>
    <div class="pure-u-1-4">
        <div class="panel">
            <div class="title black text-center">
                <?= $account->getName() ?>
            </div>
            <div class="description white">
                <a href="<?= App::asset('user/account') ?>" class="pure-button button-success">< <?= Language::getWord('return', 'global', 'return')?></a><br><br>
                <?= Language::getWord('Rang', 'user', 'rang') ?>: <?php
                if($account->isGranted('IsModerator'))
                    echo Language::getWord('Moderator', 'user', 'rang1');
                else
                    echo Language::getWord('Player', 'user', 'rang0');
                ?><br>
                <?= Language::getWord('Email', 'global', 'email') ?>: <?= $account->getMail() ?><br>
                <?= Language::getWord('Number characters', 'user', 'account', 'numberCharacter') ?>: <?= count($account->getCharacter()) ?><br><br>
                <a href="<?= App::asset('user/logout') ?>" class="pure-button button-secondary"><?= Language::getWord('Logout', 'user', 'logout') ?></a>
            </div>
        </div>
        <div class="panel">
            <div class="title black text-center">
                <?= Language::getWord('Ranking', 'user', 'character', 'ranking') ?>
            </div>
            <div class="description white no-padding">
                <?php
                    $ranking = Character::ranking($character->getId());
                ?>
                <table class="pure-table pure-table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th><?= Language::getWord('Name', 'global', 'name') ?></th>
                            <th><?= $ranking['table'][0] ?></th>
                            <th><?= $ranking['table'][1] ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $index = 1;
                            $ownOK = false;
                            foreach($ranking['results'] as $result){
                                if($character->getId() == $result[0] && !$ownOK) $ownOK = true;
                                ?>
                                <tr <?= ($character->getId() == $result[0] ? 'class="select"': '')?> >
                                    <td><?= $index ?></td>
                                    <td><?= $result[1] ?></td>
                                    <td><?= $result[2] ?></td>
                                    <td><?= $result[3] ?></td>
                                </tr>
                                <?php
                                $index++;
                            }
                            if(!$ownOK){
                                ?>
                                <tr class="pure-table-striped">
                                    <td>-</td>
                                    <td><?= $ranking['own'][1] ?></td>
                                    <td><?= $ranking['own'][2] ?></td>
                                    <td><?= $ranking['own'][3] ?></td>
                                </tr>
                                <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="pure-u-1-2">
        <div class="panel">
            <div class="title black text-center">
                <div class="float-right">
                    <!--<a href="removeCharacter?id=<?= $character->getId() ?>" class="pure-button button-error"><i class="fa fa-trash"></i></a>-->
                </div>
                <b><?= $character->getName() ?></b>
            </div>
            <div class="description white">
                <b><?= Language::getWord('Level', 'user', 'character', 'level') ?>: </b><?= $character->getLevel() ?><br>
                <b><?= Language::getWord('Experience', 'user', 'character', 'experience') ?>: </b><?= $character->getXp() ?><br><br>

                <b><?= Language::getWord('Position', 'user', 'character', 'position') ?>: </b>
                <?= Language::getWord('X', 'user', 'character', 'x') ?>: <?= $character->getPosition()['X'] ?>;
                <?= Language::getWord('Y', 'user', 'character', 'y') ?>: <?= $character->getPosition()['Y'] ?>;
                <?= Language::getWord('Z', 'user', 'character', 'z') ?>: <?= $character->getPosition()['Z'] ?><br>
                <b><?= Language::getWord('Location map', 'user', 'character', 'locationMap') ?>: </b><?= $character->getMapID()->getName() ?><br><br>
                <b><?= Language::getWord('Last Connected', 'user', 'lastConnected') ?>: </b><?= Check::timeString($character->getLastOnline()) ?><br><br>

                <div class="pure-g">
                    <div class="pure-u-1-2">
                        <b><?= Language::getWord('Inventory', 'user', 'character', 'inventory') ?>:</b><br>
                        <table class="pure-table pure-table-bordered">
                            <thead>
                            <tr>
                                <th><?= Language::getWord('Name', 'global', 'name') ?></th>
                                <th><?= Language::getWord('Number', 'user', 'character', 'number') ?></th>
                                <th><?= Language::getWord('Equipped', 'user', 'character', 'equipped') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            /** @var Inventory $inventory */
                            foreach($character->getInventory() as $inventory){
                                ?>
                                <tr>
                                    <td><?= $inventory->getItem()->getName() ?></td>
                                    <td><?= $inventory->getNumber() ?></td>
                                    <td><?= $inventory->getEquipped() ? Language::getWord('Yes', 'global', 'yes') : Language::getWord('No', 'global', 'no') ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="pure-u-1-2">
                        <b><?= Language::getWord('Variable', 'user', 'character', 'variable') ?>:</b><br>
                        <table class="pure-table pure-table-bordered">
                            <thead>
                            <tr>
                                <th><?= Language::getWord('Name', 'global', 'name') ?></th>
                                <th><?= Language::getWord('Value', 'user', 'character', 'value') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            /** @var CharacterVariables $variable */
                            foreach($character->getVariables() as $variable){
                                if($variable->getVariable()->getHidden()) continue;
                                ?>
                                <tr>
                                    <td><?= $variable->getVariable()->getName() ?></td>
                                    <td><?= $variable->getValue() ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <b><?= Language::getWord('Quests', 'user', 'character', 'quests') ?>:</b><br>
                <table class="pure-table pure-table-bordered">
                    <thead>
                    <tr>
                        <th><?= Language::getWord('Name', 'global', 'name') ?></th>
                        <th><?= Language::getWord('Tasks', 'user', 'character', 'tasks') ?></th>
                        <th><?= Language::getWord('Requirements', 'user', 'character', 'requirements') ?></th>
                        <th><?= Language::getWord('Finish', 'user', 'character', 'finish') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    /** @var CharacterQuest $quest */
                    foreach($character->getQuests()  as $quest){
                        ?>
                        <tr>
                            <td><?= $quest->getQuest()->getName() ?></td>
                            <td>
                                <?php
                                    foreach($quest->getQuest()->getTasks() as $task){
                                        if($task['Description'] != ""){
                                            echo '- '.$task['Description'].'<br>';
                                        }
                                        $target = $task['TargetId'];
                                        if($target instanceof Npc) {
                                            echo '['. Language::getWord( 'Kill', 'user', 'character', 'kill' ) .'] [' . $task['Quantity'] .'/'.$task['Objective'].'] '.$target->getName().'<br>';
                                        } else if($target instanceof Item) {
                                            echo '['. Language::getWord( 'Collect', 'user', 'character', 'collect' ) .'] ['.$task['Quantity'].'/'.$task['Objective'].'] '.$target->getName().'<br>';
                                        } else if($target instanceof Event) {
                                            echo '['. Language::getWord('Event', 'user', 'character', 'event') . '] ['.$task['Quantity'].'/'.$task['Objective'].']'.$target->getName(). '<br>';
                                        }
                                        echo '<br>';
                                    }
                                ?>
                            </td>
                            <td>
                                <?php
                                foreach($quest->getQuest()->getRequirements() as $requirement){
                                        echo '- '.$requirement['Name'].'<br>';
                                    }
                                    if(count($quest->getQuest()->getRequirements())==0){
                                        echo Language::getWord( 'No', 'global', 'no' );
                                    }
                                ?>
                            </td>
                            <td><?= $quest->getCompleted() != "0" ? Language::getWord( 'Yes', 'global', 'yes' ) : Language::getWord( 'No', 'global', 'no' ) ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>