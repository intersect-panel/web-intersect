<?php
/** @var User $account */
$account = App::getVar('account');
?>
<div class="pure-g marge">
    <div class="pure-u-1-6"></div>
    <div class="pure-u-1-3">
        <div class="panel">
            <div class="title black text-center">
                <?= $account->getName() ?>
            </div>
            <div class="description white">
                Rang: <?php
                if($account->isGranted('IsModerator'))
                    echo Language::getWord('Moderator', 'user', 'rang1');
                else
                    echo Language::getWord('Player', 'user', 'rang0');
                ?><br>
                <?= Language::getWord('Email', 'global', 'email')?>:<?= $account->getMail() ?><br>
                <?= Language::getWord('Number characters', 'user', 'account', 'numberCharacter')?>: <?= count($account->getCharacter()) ?><br>
                <br>
                <?php
                    if($account->isGranted('IsModerator')){
                        ?>
                        <a href="<?= App::asset('admin/index') ?>" class="pure-button button-secondary"><?= Language::getWord('Panel Admin', 'user', 'panelAdmin')?></a>
                        <?php
                    }
                ?>
                <a href="<?= App::asset('user/logout') ?>" class="pure-button button-secondary"><?= Language::getWord('Logout', 'user', 'logout')?></a>
            </div>
        </div>
    </div>
    <div class="pure-u-1-3">
        <div class="vertical-text">
            <div class="text-center">
                <div class="panel">
                    <div class="description white">
                        <form method="post" action="<?= App::asset('user/changePassword') ?>" class="pure-form pure-form-stacked">
                            <fieldset>
                                <label for="password"><?= Language::getWord('Change your password:', 'user', 'account', 'changePassword')?></label>
                                <input id="password" type="password" name="password">
                                <label for="email"><?= Language::getWord('Change your email:', 'user', 'account', 'changeEmail')?></label>
                                <input id="email" type="email" name="email">
                                <input type="submit" class="pure-button button-success" value="<?= Language::getWord('Save', 'global', 'save')?>">
                            </fieldset>
                        </form>
                    </div>
                </div>

                <div class="panel">
                    <div class="description white">
                        <?php
                        if(count($account->getCharacter()) == 0)
                            echo "No character";
                        /** @var Character $char */
                        foreach ($account->getCharacter() as $char){
                            if(count($account->getCharacter()) == 1){
                                ?>
                                <a href="<?= App::asset('user/character') ?>" class="pure-button pure-button-primary"><i class="fa fa-eye"></i></a>
                                <?php
                            }else{
                                ?>
                                <a href="<?= App::asset('user/character') ?>?id=<?= $char->getId() ?>" class="pure-button pure-button-primary"><i class="fa fa-eye"></i></a>
                                <?php
                            }
                        echo $char->getName().'('.$char->getLevel().')';
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
