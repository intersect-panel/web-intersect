<div class="pure-g marge">
    <div class="pure-u-1-8"></div>
    <div class="pure-u-3-4">
        <div class="panel">
            <div class="description">
                <a href="<?= App::asset('admin/index')?>" class="pure-button button-secondary">< <?= Language::getWord('Return', 'global', 'return') ?></a>
            </div>
        </div>
        <div class="panel">
            <div class="title black">
                <?= Language::getWord('List of items', 'admin', 'users', 'name') ?>
            </div>
            <div class="description white">
                <?php
                if(Router::get('page', 0) > 0){
                    ?>
                    <a href="<?= App::asset('admin/items')?>?page=<?= (Router::get('page')-1).""?>" class="pure-button button-secondary">< <?= Language::getWord('Previous', 'admin', 'previous') ?></a>
                    <?php
                }elseif(count(App::getVar('items')) == 25){
                    ?>
                    <div class="text-right">
                        <a href="<?= App::asset('admin/items')?>?page=<?= (Router::get('page', 0)+1).""?>" class="pure-button button-secondary"><?= Language::getWord('Next', 'admin', 'next') ?> ></a>
                    </div>
                    <?php
                }
                ?>
                <table class="pure-table pure-table-horizontal">
                    <thead>
                    <tr>
                        <th><?= Language::getWord('ID', 'global', 'id') ?></th>
                        <th><?= Language::getWord('Name', 'global', 'name') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    /** @var Item $item */
                    foreach(App::getVar('items') as $item){
                        ?>
                        <tr>
                            <td><?= $item->getId() ?></td>
                            <td><?= $item->getName() ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>