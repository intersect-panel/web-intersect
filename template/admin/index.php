<div class="pure-g marge">
    <div class="pure-u-1-8"></div>
    <div class="pure-u-3-4">
        <div class="panel">
            <div class="title black">
                <?= Language::getWord('Page administration', 'admin', 'index', 'name') ?>
            </div>
            <div class="description white">
                <a href="<?= App::asset('user/account')?>" class="pure-button button-secondary">< <?= Language::getWord('Return', 'global', 'return') ?></a><br>
                <a href="<?= App::asset('admin/users')?>" class="pure-button button-secondary"><?= Language::getWord('List User', 'admin', 'index', "listUser") ?></a>
                <a href="<?= App::asset('admin/maps')?>" class="pure-button button-secondary"><?= Language::getWord('List Maps', 'admin', 'index', "listMap") ?></a>
                <a href="<?= App::asset('admin/bans')?>" class="pure-button button-secondary"><?= Language::getWord('List Ban', 'admin', 'index', "listBan") ?></a>
                <a href="<?= App::asset('admin/items')?>" class="pure-button button-secondary"><?= Language::getWord('List Item', 'admin', 'index', "listItem") ?></a>
                <a href="<?= App::asset('admin/npcs')?>" class="pure-button button-secondary"><?= Language::getWord('List Npcs', 'admin', 'index', "listNpc") ?></a>
                <a href="<?= App::asset('admin/config')?>" class="pure-button button-secondary"><?= Language::getWord('Edit config.json', 'admin', 'index', "configJSON") ?></a>
                <?php
                    if(Server::intersectValue('', 'nameRoot') === '' && Server::intersectValue('', 'intersect_folder_server') != ''){
                        ?>
                        <br><a href="<?= App::asset('server/install') ?>" class="pure-button button-warning"><?= Language::getWord('Configure Server Intersect', 'admin', 'index', 'installServer') ?></a>
                        <?php
                    }
                ?>
            </div>
        </div>
        <div class="panel">
            <div class="description white">
                <div class="pure-g">
                    <div class="pure-u-1">
                        IP Server <?= file_get_contents('https://ipecho.net/plain') ?><br>
                        <?php
                        if(Server::intersectValue('', 'nameRoot') != '' && Server::intersectValue('', 'intersect_folder_server') != '') {
                            $serStatut = shell_exec( 'netstat -a -o -n -p UDP | findstr /i ' . Server::intersectValue( 5400, 'config_server', 'ServerPort' ) );
                            if ($serStatut == '') {
                                ?>
                                Serveur off<br>
                                <a href="<?= App::asset( 'server/start' ) ?>" class="pure-button button-success">Start
                                    server</a>
                                <?php
                            } else {
                                ?>
                                Serveur on<br>
                                <a href="<?= App::asset( 'server/stop' ) ?>" class="pure-button button-error">Stop
                                    server</a>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <?php
                        if(($linfo = App::getVar('linfo', null)) !== null){
                            $ram = $linfo->getRam();
                            $cpu = $linfo->getCpu();
                            $ramPercent = ($ram['free']/intval($ram['total']))*100;
                            $indexC = 0;
                            foreach($cpu as $c){
                                ?>
                                <div class="pure-u-1-4">
                                    <div>
                                        <b>CPU ID: </b><?= $indexC ?><br>
                                        <b>Frequency: </b><?= $c['MHz'] ?> MHz
                                    </div>
                                    <div class="ct-chart ct-perfect-fourth" id="cpu<?= $indexC ?>"></div>
                                    <script>
                                        new Chartist.Pie('#cpu<?= $indexC ?>', {
                                            series: [ <?= round($c['usage_percentage'],2) ?>, <?= round(100-$c['usage_percentage'],2) ?>]
                                        }, {
                                            donut: true,
                                            donutWidth: 30,
                                            donutSolid: true,
                                            startAngle: 270,
                                            total: 200,
                                            showLabel: true
                                        });
                                    </script>
                                </div>
                                <?php
                                $indexC++;
                            }
                            ?>
                            <div class="pure-u-1-4"></div>
                            <div class="pure-u-1-4">
                                <div>
                                    <b>RAM: </b><?= round(intval($ram['total'])/1024/1024, 2) ?> MB<br>
                                    <b>Type: </b><?= $ram['type'] ?>
                                </div>
                                <div class="ct-chart ct-perfect-fourth" id="ram"></div>
                                <script>
                                    new Chartist.Pie('#ram', {
                                        series: [ <?= round($ramPercent/2,2) ?>, <?= round(100-$ramPercent/2,2) ?>]
                                    }, {
                                        donut: true,
                                        donutWidth: 30,
                                        donutSolid: true,
                                        startAngle: 270,
                                        total: 200,
                                        showLabel: true
                                    });
                                </script>
                            </div>
                            <div class="pure-u-1-4">
                                <?php
                                    if(key_exists('swapTotal', $ram)){
                                        $swapPercent = $ram['swapFree']/$ram['swapTotal']*100;

                                        ?>

                                        <div class="ct-chart ct-perfect-fourth" id="swap"></div>
                                        <script>
                                            new Chartist.Pie('#swap', {
                                                series: [ <?= round($swapPercent/2,2) ?>, <?= round(100-$swapPercent/2,2) ?>]
                                            }, {
                                                donut: true,
                                                donutWidth: 30,
                                                donutSolid: true,
                                                startAngle: 270,
                                                total: 200,
                                                showLabel: true
                                            });
                                        </script>
                                        <?php
                                    }else{
                                        echo '<b>No SWAP installed</b>';
                                    }
                                ?>
                            </div>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>