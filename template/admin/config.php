<div class="pure-g marge">
    <div class="pure-u-1-8"></div>
    <div class="pure-u-3-4">
        <div class="panel">
            <div class="description">
                <a href="<?= App::asset('admin/index')?>" class="pure-button button-secondary">< <?= Language::getWord('Return', 'global', 'return') ?></a>
            </div>
        </div>
        <div class="panel">
            <div class="title black">
                <?= Language::getWord('Configuration JSON', 'admin', 'config', 'name') ?>
            </div>
            <div class="description white">
                <form method="post" action="<?= App::asset('admin/configSave')?>" class="pure-form pure-form-stacked">
                    <?php
                    $v = preg_split('/!/',App::getVar( 'nameArr' ));
                    if($v[0] != "")
                        if(count($v) == 1){
                            ?>
                            <a href="<?= App::asset('admin/config') ?>" class="pure-button button-secondary">< <?= Language::getWord('Return', 'global', 'return') ?> JSON</a>
                            <?php
                        }else{
                            $ve = substr(Router::get('array'), 0, strrpos(Router::get('array'), '!'));
                            ?>
                            <a href="<?= App::asset('admin/config') ?>?array=<?= $ve ?>" class="pure-button button-secondary">< <?= Language::getWord('Return', 'global', 'return') ?> <?= $v[count($v)-2] ?></a>
                            <?php
                        }
                    ?>
                    <fieldset>
                        <input type="hidden" name="array" value="<?= App::getVar('nameArr') ?>" />
                        <?php
                            $save = false;
                            foreach(App::getVar('config') as $key => $value){
                                if(is_array($value)){
                                    $n = App::getVar( 'nameArr' );
                                    if($n == "")
                                        $n = $key;
                                    else
                                        $n .= '!'.$key;
                                    $name = "";
                                    if(key_exists('name', $value))
                                        $name = $value['name'];
                                    ?>
                                    <?= $key.($name != "" ? " - ".$name:"") ?> <br>
                                    <a href="<?= App::asset('admin/config') ?>?array=<?= $n ?>" class="pure-button button-secondary"><?= Language::getWord('Edit', 'global', 'edit') ?></a><br>
                                    <?php
                                }else{
                                    $save = true;
                                    ?>
                                    <label for="<?= $key ?>"><?= $key?></label>
                                    <input id="<?= $key ?>" type="text" placeholder="<?= ($value === false) ? 'false': ($value===true)?'true':$value ?>" name="config[<?= $key ?>]" value="<?= ($value===false) ? 'false' : ($value===true)?'true':$value ?>">
                                    <?php
                                }
                            }
                            if($save){
                        ?>
                        <input type="submit" class="pure-button button-success" value="<?= Language::getWord('Save', 'global', 'save') ?>">
                        <?php } ?>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>