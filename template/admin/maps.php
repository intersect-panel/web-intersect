<div class="pure-g marge">
    <div class="pure-u-1-8"></div>
    <div class="pure-u-3-4">
        <div class="panel">
            <div class="description">
                <a href="<?= App::asset('admin/index')?>" class="pure-button button-secondary">< <?= Language::getWord('Return', 'global', 'return') ?></a>
            </div>
        </div>
        <div class="panel">
            <div class="title black">
                <?= Language::getWord('List of maps', 'admin', 'maps', 'name') ?>
            </div>
            <div class="description white">
                <?php
                if(Router::get('page', 0) > 0){
                    ?>
                    <a href="<?= App::asset('admin/maps')?>?page=<?= (Router::get('page')-1).""?>" class="pure-button button-secondary">< <?= Language::getWord('Previous', 'admin', 'previous') ?></a>
                    <?php
                }elseif(count(App::getVar('maps')) == 25){
                    ?>
                    <div class="text-right-right">
                        <a href="<?= App::asset('admin/maps')?>?page=<?= (Router::get('page', 0)+1).""?>" class="pure-button button-secondary"><?= Language::getWord('Next', 'admin', 'next') ?> ></a>
                    </div>
                    <?php
                }
                ?>
                <table class="pure-table pure-table-horizontal">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><?= Language::getWord('Name', 'global', 'name') ?></th>
                        <th><?= Language::getWord('Zone Type', 'admin', 'maps', 'zoneType') ?></th>
                        <th><?= Language::getWord('Events', 'admin', 'maps', 'event') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    /** @var Map $map */
                    foreach(App::getVar('maps') as $map){
                        ?>
                        <tr>
                            <td><?= $map->getId() ?></td>
                            <td><?= $map->getName() ?></td>
                            <td><?= $map->getZoneTpe() ?></td>
                            <td>
                                <?php foreach($map->getEvents() as $event){ ?>
                                    - <?= $event ?><br>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>