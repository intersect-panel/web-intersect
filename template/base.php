<?php
$start = $GLOBALS['start'];
?>
<html lang="<?= substr($_SESSION['lang'], 0, (($pos = strpos($_SESSION['lang'], '-')) > 1 ? $pos : null))?>">
    <head>
        <title>
            <?= Router::websiteValue("Intersect Panel", 'title') ?>
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <link href="//fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link href="<?= App::asset('css/pure.css') ?>" type="text/css" rel="stylesheet">
        <link href="<?= App::asset('css/design.css') ?>" type="text/css" rel="stylesheet">
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/all.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.css">
        <script src="//cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>
        <?php
            if(Router::websiteValue( '', 'recaptcha_key_site') != '')
                echo '<script src="https://www.google.com/recaptcha/api.js" async defer></script>';

        ?>
    </head>
    <body>
        <?php
            if(count(App::alert()) > 0){
        ?>
            <div class="alert-float">
                <?php
                foreach(App::alert() as $v)
                    foreach($v as $key => $value) { ?>
                    <div class="alert-<?= $key ?>">
                        <div class="float-right" onclick="this.parentNode.remove();" style="margin-right: 5px;"><i class="fa fa-times"></i></div>
                        <?= $value ?>
                    </div>
                <?php } ?>
            </div>
        <?php App::cleanAlert(); } ?>
        <div class="information">
            <div class="float-right language">
                <?php
                    foreach(Language::getLanguages() as $language){?>
                        <a href="<?= App::asset('language') ?>?key=<?= $language['key'] ?>&page=<?= Router::URL()?>"><img src="<?= App::asset($language['image']) ?>" title="<?= $language['name'] ?>"  alt="<?= $language['name'] ?>"></a>
                    <?php
                    }
                ?>
            </div>
            <div>
                <?= App::getVar('account', null) !== null ? Router::getValueArray(Server::intersect(), '-', 'name') : "-" ?>
            </div>
        </div>
        <div class="container">
            <?php include_once App::template() ?>
        </div>
        <div class="timeInfo">
            <span>
                <?php
                $time = explode(' ', microtime());
                $finish = $time[1] + $time[0];
                $total_time = round(($finish - $start), 4);
                echo Language::getWord( 'Generated page in ', 'index', 'base', 'pageGenerated' ).' '.$total_time.'s';
                if(App::getVar('account', null) !== null)
                    echo ' - '.Language::getWord( 'Number of SQL query: ', 'index', 'base', 'numberQuery' ).Database::$nbrSQL.'/'.Database::$nbrCache;
                ?>
            </span>
        </div>
        <div class="copyright">
            <span>
                <?= Router::websiteValue('Copyright Missing !','copyright') ?>
            </span>
        </div>
    </body>
</html>
