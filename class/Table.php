<?php


abstract class Table implements Itable
{
    protected $smallInit;
    protected $checkOK = true;
    protected $blob;
    public function __construct($blob = array('Id'))
    {
        $this->blob = $blob;
    }

    /**
     * @param $id
     * @param $class
     * @param $tableName
     * @param null $db
     * @param bool $smallInit
     * @return Table|null
     */
    public static function Id($id, $class, $tableName, $db = null, $smallInit = false){
        if($db != null)
            Database::setDB($db);

        $nv = new $class();
        if(!$nv instanceof Table) {
            echo $class." isn't implement 'Itable' interface";
            die();
        }

        $row = Database::getRow($tableName, array('Id='=>$id), '', '', $nv->getBlob());
        unset($nv);

        if($row == null) return null;
        return self::createClass($row, $class, $smallInit);
    }

    protected static function createClass($row, $class, $smallInit = false)
    {
        $nv = new $class();
        if(!$nv instanceof Table) {
            echo $class." isn't implement 'Itable' interface";
            die();
        }
        $nv->smallInit = $smallInit;

        foreach(get_class_vars($class) as $k => $v){
            foreach($row as $key => $value){
                if(strtolower($k) == strtolower($key)){
                    $nv->{$k} = $value;
                    break;
                }
            }
        }
        $nv->init();
        return ($nv->isCheck()) ? $nv : null;
    }

    protected function destroy(){
        $this->checkOK = false;
    }

    protected function isCheck(){
        return $this->checkOK;
    }

    protected function getBlob(){
        return $this->blob;
    }

}