<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 28/03/2019
 * Time: 17:16
 */

class User extends Table
{
    protected $id;
    public $name;
    public $email;
    public $power;
    public $character;
    protected $password;
    protected $salt;
    protected $ban;

    function init()
    {
        $this->password = "";
        $this->ban = Ban::getBy(array('PlayerId=' => $this->id));
        foreach(Database::getRows('characters', array('AccountId=' => $this->id)) as $character) {
            $this->character[] = Character::createClass( $character, Character::class );
        }
        $this->power = json_decode($this->power, true);
        $this->power['IsUser'] = true;
    }

    public static function login($account, $password){
        if(Check::mail($account))
            $result = Database::getRows('users', array("Email=" => $account));
        elseif(Check::alpha($account))
            $result = Database::getRows('users', array("Name=" => $account));
        else
            return -1;

        if(count($result) == 1){
            $user = $result[0];
            $pass = strtoupper(hash('sha256', strtoupper($password).$user['Salt']));
            if($pass == $user['Password']){
                return $user['Id'];
            }
            return -1;
        }
        return -1;
    }

    public static function all($start, $step = 25){
        $list = array();
        foreach(Database::getRows('users', array(), '', $start*$step.', '.$step) as $row){
            $user = User::createClass($row, User::class);
            $list[] = $user;
        }
        return $list;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getMail()
    {
        return $this->email;
    }

    /**
     * @param string $mail
     */
    public function setMail($mail)
    {
        $this->email = $mail;
        Database::updateRow("users", array('Id' => $this->getId()), array('Email' => $mail));
    }

    /**
     * @return array
     */
    public function getPower()
    {
        return $this->power;
    }

    /**
     * @param mixed $power
     */
    public function setPower($power)
    {
        $this->power = $power;
    }

    /**
     * @return array
     */
    public function getCharacter()
    {
        return $this->character;
    }

    /**
     * @param $id
     * @return Character
     */
    public function getCharacterId($id){
        /** @var Character $character */
        foreach ($this->getCharacter() as $character){
            if($character->getId() == $id)
                return $character;
        }
        return $this->getCharacter()[0];
    }

    public function deleteCharacterId($id){
        $c = null;
        /** @var Character $character */
        foreach ($this->getCharacter() as $character){
            if($character->getId() == $id){
                $c = $character;
                break;
            }
        }
        if($c == null) return false;
        Database::deleteRow('character_bank', array('CharacterId' => $c->getId()));
        Database::deleteRow('character_friends', array('OwnerId' => $c->getId()));
        Database::deleteRow('character_friends', array('TargetId' => $c->getId()));
        Database::deleteRow('character_hotbar', array('CharacterId' => $c->getId()));
        Database::deleteRow('character_items', array('CharacterId' => $c->getId()));
        Database::deleteRow('character_quests', array('CharacterId' => $c->getId()));
        Database::deleteRow('character_spells', array('CharacterId' => $c->getId()));
        Database::deleteRow('character_switches', array('CharacterId' => $c->getId()));
        Database::deleteRow('character_variables', array('CharacterId' => $c->getId()));
        Database::deleteRow('characters', array('Id' => $c->getId()));
        return true;
    }

    /**
     * @param array $character
     */
    public function setCharacter($character)
    {
        $this->character = $character;
    }

    /**
     * @param Character $character
     */
    public function addCharacter($character){
        $this->character[] = $character;
    }

    /**
     * @param string $r
     * @return bool
     */
    public function isGranted($r){
        foreach($this->getPower() as $role => $value){
            if($role == $r){
                return $value;
            }
        }
        return false;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
        Database::updateRow("users", array('Id' => $this->getId()), array('Password' => $password));
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }
}