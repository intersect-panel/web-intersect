<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 29/03/2019
 * Time: 12:56
 */

class Character extends Table
{
    protected $id;
    public $x;
    public $y;
    public $z;
    public $dir;
    public $name;
    public $mapID;
    public $position;
    public $sprite;
    public $face;
    public $level;
    public $gender;
    public $exp;
    public $equipment;
    public $lastOnline;
    public $quests;
    public $inventory;
    public $variables;

    function init()
    {
        $this->equipment = json_decode($this->equipment, true);

        $this->inventory = array();
        $this->quests = array();
        $this->variables = array();
        $this->position = array('X'=>$this->x, 'Y'=>$this->y, 'Z'=>$this->z, 'Dir'=>$this->dir);
        foreach(Database::getRows('character_variables', array('CharacterId='=>$this->id)) as $row){
            $v = CharacterVariables::createClass($row, CharacterVariables::class);
            if($v != null){
                $this->variables[] = $v;
            }
        }

        if(!$this->smallInit){
            $this->mapID = Map::id($this->getMapID(), Map::class, 'Maps', 'GameDatabase');

            foreach (Database::getRows('character_items', array('CharacterId=' => $this->id)) as $row) {
                if (substr($row['ItemId'], 0, 6) != "000000") {
                    $this->inventory[] = Inventory::createClass( $row, Inventory::class );
                }
            }

            foreach(Database::getRows('character_quests', array('CharacterId=' => $this->id)) as $row){
                $v = CharacterQuest::createClass( $row, CharacterQuest::class);
                if($v != null){
                    $this->quests[] = $v;
                }
            }
        }
    }

    public static function ranking($id, $limit = 25){
        $rankingCharacter = array();
        $variables = Server::intersectValue( null, "ranking");
        if($variables == null) return $rankingCharacter;
        $ownC = null;
        $head = [];
        $t0 = $variables['0'];
        $t1 = $variables['1'];

        if(count($head) == 0){
            if($t0['type'] == "variable") {
                /** @var PlayerVariable $playerV */
                $playerV = PlayerVariable::getVariableTextId($t0['id']);
                $head[] = $playerV->getName();
            }else{
                $head[] = $t0['type'];
            }
            if($t1['type'] == "variable") {
                $playerV = PlayerVariable::getVariableTextId($t1['id']);
                $head[] = $playerV->getName();
            }else{
                $head[] = $t1['type'];
            }
        }
        $cacheRank = App::getCache('ch/','ranking');
        if($cacheRank == null){
            foreach(Database::getRows("characters") as $row){
            /** @var Character $character */
            $character = Character::createClass($row, Character::class, true);

            if($t0['type'] == "variable"){
                $v0Value = $character->getVariablesValueID($variables['0']['id']);
            }else{
                $v0Value = $character->{$t0['type']};
            }
            if($t1['type'] == "variable"){
                $v1Value = $character->getVariablesValueID($variables['1']['id']);
            }else{
                $v1Value = $character->{$t1['type']};
            }

            $newC = array($character->getId(), $character->getName(), $v0Value, $v1Value);
            if($character->getId() == $id && $ownC == null)
                $ownC = $newC;

            $inside = false;
            $index = 0;
            foreach($rankingCharacter as $rc){
                if($v0Value == $rc[2]){
                   if($v1Value >= $rc[3]){
                       array_splice($rankingCharacter, $index, 0, [$newC]);
                       $inside = true;
                   }
                }elseif($v0Value > $rc[2]){
                    array_splice($rankingCharacter, $index, 0, [$newC]);
                    $inside = true;
                }
                if($inside) break;
                $index++;
            }

            if(!$inside)
                $rankingCharacter[] = $newC;
        }
            App::setCache('ch/', 'ranking', $rankingCharacter);
        }else{
            $rankingCharacter = $cacheRank;
            foreach($rankingCharacter as $rC){
                if($rC[0] == $id){
                    $ownC = $rC;
                    break;
                }

            }
        }
        $rankingCharacter = array_slice($rankingCharacter, 0, $limit);
        return array("table"=>$head, "results"=>$rankingCharacter, "own"=>$ownC);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Map
     */
    public function getMapID()
    {
        return $this->mapID;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return mixed
     */
    public function getSprite()
    {
        return $this->sprite;
    }

    /**
     * @return mixed
     */
    public function getFace()
    {
        return $this->face;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @return mixed
     */
    public function getXp()
    {
        return $this->exp;
    }


    /**
     * @return mixed
     */
    public function getEquipment()
    {
        return $this->equipment;
    }

    /**
     * @return mixed
     */
    public function getLastOnline()
    {
        return $this->lastOnline;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getInventory(){
        return $this->inventory;
    }

    /**
     * @return array
     */
    public function getQuests()
    {
        return $this->quests;
    }

    /**
     * @return array
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * @param $textId
     * @param int $defaultValue
     * @return int
     */
    public function getVariablesValueID($textId, $defaultValue = 0){
        /** @var CharacterVariables $variable */
        foreach($this->variables as $variable){
            if($variable->getVariable()->textId == $textId){
                return $variable->getValue();
            }
        }
        return $defaultValue;
    }

}