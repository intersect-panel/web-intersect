<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 01/04/2019
 * Time: 18:39
 */

class Inventory extends Table
{
    protected $id;
    public $itemId;
    public $quantity;
    public $slot;
    public $equipped;

    function init()
    {
        $this->itemId = Item::Id($this->itemId, Item::class, 'Items', 'GameDatabase');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->itemId;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->quantity;
    }

    /**
     * @return mixed
     */
    public function getSlot()
    {
        return $this->slot;
    }

    /**
     * @return mixed
     */
    public function getEquipped()
    {
        return $this->equipped;
    }
}