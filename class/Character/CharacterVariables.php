<?php


class CharacterVariables extends Table
{
    protected $id;
    public $variableID;
    public $value;
    public $raw;

    function init()
    {
        $this->raw = array(
            'Value'=>$this->value
        );

        $this->variableID = PlayerVariable::Id($this->variableID, PlayerVariable::class, 'PlayerVariables', 'GameDatabase');
        if($this->variableID == null) {
            $this->destroy();
            return;
        }

        $valueI = Server::intersectValue(null,'playerVariables', $this->getVariable()->getTextId(), "values", $this->value);
        if($valueI !== null)
            $this->value = $valueI;
    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return PlayerVariable
     */
    public function getVariable()
    {
        return $this->variableID;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }



}