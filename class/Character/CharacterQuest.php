<?php


class CharacterQuest extends Table
{
    protected $id;
    public $questId;
    public $completed;
    public $task;

    function init(){
        $this->questId = Quest::Id($this->questId, Quest::class, 'Quests', 'GameDatabase');
        if($this->questId == null){
            $this->destroy();
            return;
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Quest
     */
    public function getQuest()
    {
        return $this->questId;
    }

    /**
     * @return mixed
     */
    public function getCompleted()
    {
        return $this->completed;
    }

    /**
     * @return array
     */
    public function getTask()
    {
        return $this->task;
    }


}