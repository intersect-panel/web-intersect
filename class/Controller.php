<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 30/03/2019
 * Time: 11:48
 */

abstract class Controller implements IController
{
    /** @var App */
    private $app;
    private $user;
    protected $templateBase;

    function __construct($isGranted = null)
    {
        $this->app = App::getInstance();
        $this->templateBase = 'base.php';
        if($isGranted != null){
            if($this->getUser() == null || !$this->getUser()->isGranted($isGranted)) {
                App::addAlert('error', 'Need identification');
                $this->redirect( 'index' );
            }
        }

    }

    /**
     * @return User
     */
    function getUser(){
        if($this->user == null)
            $this->user = App::getVar('account', null);

        return $this->user;
    }

    function redirect($url, $get = array()){
        if(count($get) != 0){
            $url .= "?";
            foreach ($get as $key => $value){
                $url .= $key."=".$value."&";
            }
            $url = substr($url, 0, -1);
        }
        header('Location: '.App::asset($url));
        return 1;
    }

    function render($t){
        $this->app->setTemplate($t);
        include_once 'template/'.$this->templateBase;
        return 1;
    }
}