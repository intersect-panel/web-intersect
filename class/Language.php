<?php


class Language
{
    private static $alLanguage = array();
    private static $languageSelect;

    public static function init(){
        if(self::$languageSelect != null)return;
        if(empty($_SESSION['lang'])) $_SESSION['lang']=Router::websiteValue('en-us', 'language');

        foreach (glob(Router::dir().'/language/*.json') as $filename){
            $arr = json_decode(file_get_contents($filename), true);
            if(Router::getValueArray($arr, 'en-us', 'config', 'key') == $_SESSION['lang'] && self::$languageSelect == null)
                self::$languageSelect = $arr;
            self::$alLanguage[] = $arr['config'];
        }
    }

    public static function getLanguages(){
        return self::$alLanguage;
    }
    public static function getWord($defaultValue, ... $table){
        return Router::getValueArray(self::$languageSelect, $defaultValue, $table).(self::$languageSelect['config']['space']?' ':'');
    }
}