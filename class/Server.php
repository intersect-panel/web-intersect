<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 29/03/2019
 * Time: 12:57
 */

class Server
{
    private static $intersect;
    private static $web;
    private static $allServerEnable = array();
    private static $windows = false;
    private $version;

    public function __construct()
    {
        self::$windows = strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
        $this->check();
    }

    private function check(){

    }

    public static function &intersect($arr = null){
        if($arr != null && self::$intersect == null){
            self::$intersect = $arr;
        }
        return self::$intersect;
    }

    public static function intersectValue($defaultValue, ... $arr){
        if(count($arr) > 0 && is_array($arr[0])) $arr = $arr[0];
        return Router::getValueArray(self::intersect(), $defaultValue, $arr);
    }


    public static function addServerEnable($arr){
        self::$allServerEnable[] = $arr;
    }

    /**
     * @return array
     */
    public static function getServerEnable(){
        return self::$allServerEnable;
    }

    public static function saveIntersect(){
        $ori = Router::setValueArray(Router::getConfigJSON([]), self::$intersect, 'servers', $_SESSION['server']);
        Router::saveConfigJSON($ori);
    }

    public static function isWindows(){
        return self::$windows;
    }
}