<?php


class Event extends Table
{
    protected $id;
    public $timeCreated;
    public $name;
    public $mapId;
    public $spawnX;
    public $spawnY;
    public $commonEvent;
    public $global;
    public $pages;

    function init()
    {
        $this->pages = json_decode($this->pages, true);
    }

    public static function getIDPage($id){
        Database::setDB('GameDatabase');
        foreach (Database::getRows('Events', array('Pages LIKE' => '%'.$id.'%'), '', '', array('Id', 'MapId')) as $row){
            return Event::createClass($row, Event::class);
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTimeCreated()
    {
        return $this->timeCreated;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getMapId()
    {
        return $this->mapId;
    }

    /**
     * @return mixed
     */
    public function getSpawnX()
    {
        return $this->spawnX;
    }

    /**
     * @return mixed
     */
    public function getSpawnY()
    {
        return $this->spawnY;
    }

    /**
     * @return mixed
     */
    public function getCommonEvent()
    {
        return $this->commonEvent;
    }

    /**
     * @return mixed
     */
    public function getGlobal()
    {
        return $this->global;
    }

    /**
     * @return mixed
     */
    public function c()
    {
        return $this->pages;
    }


}