<?php


class Quest extends Table
{
    private $id;
    public $name;
    public $beforeDescription;
    public $startDescription;
    public $endDescription;
    public $inProgressionDescription;
    public $logBeforeOffer;
    public $logAfterComplete;
    public $quitable;
    public $repeatable;
    public $requirements;
    public $tasks;

    function init()
    {
        $arr = array();
        $this->tasks = json_decode($this->tasks, true);
        $this->requirements = json_decode($this->requirements, true);
        if(count($this->requirements) != 0){
            foreach($this->requirements as $key => $requirement){
                foreach ($requirement['Conditions'] as $k => $v) {
                    $this->requirements[$key]['Conditions'][$k]['VariableId'] = Event::getIDPage( $v['VariableId']);
                }
            }
        }

        foreach($this->tasks as $task){
            $tId = $task['TargetId'];
            if(substr($tId, 0, 6) != "000000") {
                $task['TargetId'] = Item::Id($tId, Item::class, 'Items', 'GameDatabase');
                if($task['TargetId'] == null){
                    $task['TargetId'] = Npc::Id($tId, Npc::class, 'Npcs', 'GameDatabase');
                    if($task['TargetId'] == null){
                        $task['TargetId'] = Event::getIDPage($tId);
                    }
                }
            }
            $arr[] = $task;
        }
        $this->tasks = $arr;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getBeforeDescription()
    {
        return $this->beforeDescription;
    }

    /**
     * @return mixed
     */
    public function getStartDescription()
    {
        return $this->startDescription;
    }

    /**
     * @return mixed
     */
    public function getEndDescription()
    {
        return $this->endDescription;
    }

    /**
     * @return mixed
     */
    public function getInProgressionDescription()
    {
        return $this->inProgressionDescription;
    }

    /**
     * @return mixed
     */
    public function getLogBeforeOffer()
    {
        return $this->logBeforeOffer;
    }

    /**
     * @return mixed
     */
    public function getLogAfterComplete()
    {
        return $this->logAfterComplete;
    }

    /**
     * @return mixed
     */
    public function getQuitable()
    {
        return $this->quitable;
    }

    /**
     * @return mixed
     */
    public function getRepeatable()
    {
        return $this->repeatable;
    }

    /**
     * @return array
     */
    public function getRequirements()
    {
        return $this->requirements;
    }

    /**
     * @return array
     */
    public function getTasks()
    {
        return $this->tasks;
    }


}