<?php

class Npc extends Table
{
    protected $id;
    public $name;
    public $aggroList;
    public $attachAllies;
    public $aggressive;
    public $movement;
    public $swarm;
    public $fleeHealthPercentage;
    public $focusHighestDamageDealer;
    public $playerFriendConditions;
    public $attackOnSightConditions;
    public $playerCanAttackConditions;
    public $damage;
    public $damageType;
    public $critChance;
    public $critMultiplier;
    public $drops;
    public $experience;
    public $level;
    public $maxVital;
    public $npcVsNpcEnabled;
    public $scaling;
    public $scalingStat;
    public $sightRange;
    public $spawnDuration;
    public $spellFrequency;
    public $spells;
    public $sprite;
    public $stats;
    public $vitalRegen;

    public static function all($page = 0, $step = 25){
        $a = array();
        Database::setDB("GameDatabase");
        foreach(Database::getRows('Npcs', array(), '', $page*$step.''.$step) as $row){
            $a[] = self::createClass($row, Npc::class);
        }
        return $a;
    }

    function init()
    {
        // TODO: Implement init() method.
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getAggroList()
    {
        return $this->aggroList;
    }

    /**
     * @return mixed
     */
    public function getAttachAllies()
    {
        return $this->attachAllies;
    }

    /**
     * @return mixed
     */
    public function getAggressive()
    {
        return $this->aggressive;
    }

    /**
     * @return mixed
     */
    public function getMovement()
    {
        return $this->movement;
    }

    /**
     * @return mixed
     */
    public function getSwarm()
    {
        return $this->swarm;
    }

    /**
     * @return mixed
     */
    public function getFleeHealthPercentage()
    {
        return $this->fleeHealthPercentage;
    }

    /**
     * @return mixed
     */
    public function getFocusHighestDamageDealer()
    {
        return $this->focusHighestDamageDealer;
    }

    /**
     * @return mixed
     */
    public function getPlayerFriendConditions()
    {
        return $this->playerFriendConditions;
    }

    /**
     * @return mixed
     */
    public function getAttackOnSightConditions()
    {
        return $this->attackOnSightConditions;
    }

    /**
     * @return mixed
     */
    public function getPlayerCanAttackConditions()
    {
        return $this->playerCanAttackConditions;
    }

    /**
     * @return mixed
     */
    public function getDamage()
    {
        return $this->damage;
    }

    /**
     * @return mixed
     */
    public function getDamageType()
    {
        return $this->damageType;
    }

    /**
     * @return mixed
     */
    public function getCritChance()
    {
        return $this->critChance;
    }

    /**
     * @return mixed
     */
    public function getCritMultiplier()
    {
        return $this->critMultiplier;
    }

    /**
     * @return mixed
     */
    public function getDrops()
    {
        return $this->drops;
    }

    /**
     * @return mixed
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @return mixed
     */
    public function getMaxVital()
    {
        return $this->maxVital;
    }

    /**
     * @return mixed
     */
    public function getNpcVsNpcEnabled()
    {
        return $this->npcVsNpcEnabled;
    }

    /**
     * @return mixed
     */
    public function getScaling()
    {
        return $this->scaling;
    }

    /**
     * @return mixed
     */
    public function getScalingStat()
    {
        return $this->scalingStat;
    }

    /**
     * @return mixed
     */
    public function getSightRange()
    {
        return $this->sightRange;
    }
}