<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 30/03/2019
 * Time: 18:52
 */

class Map extends Table
{
    protected $id;
    public $name;
    public $zoneTpe;
    public $events; //JSON
    public $revision;
    public $lights;
    public $npcSpawns;
    public $sound;
    public $isIndoors;
    public $panorama;
    public $fog;
    public $fogXSpeed;
    public $fogYSpeed;
    public $fogTransparency;
    public $rHue;
    public $gHue;
    public $bHue;
    public $aHue;
    public $brightness;
    public $playerLightSize;
    public $playerLightIntensity;
    public $playerLightExpand;
    public $overlayGraphic;
    public $weatherXSpeed;
    public $weatherYSpeed;
    public $weatherIntensity;

    function init()
    {
        $this->events = json_decode($this->events, true);
    }

    public static function all($start, $step = 25){
        $re = array();
        Database::setDB("GameDatabase");
        foreach (Database::getRows('Maps', array(), '', $start*$step.', '.$step) as $row){
            $map = self::createClass($row, Map::class);
            $re[] = $map;
        }
        return $re;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getZoneTpe()
    {
        return $this->zoneTpe;
    }

    /**
     * @return mixed
     */
    public function getEvents()
    {
        return $this->events;
    }
}