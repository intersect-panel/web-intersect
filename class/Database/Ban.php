<?php


class Ban
{
    protected $id;
    public $playerID;
    public $ip;
    public $startTime;
    public $endTime;
    public $reason;
    public $banner;

    /**
     * Ban constructor.
     * @param $id
     * @param $playerID
     * @param $ip
     * @param $startTime
     * @param $endTime
     * @param $reason
     * @param $banner
     */
    public function __construct($id, $playerID, $ip, $startTime, $endTime, $reason, $banner)
    {
        $this->id = $id;
        $this->playerID = $playerID;
        $this->ip = $ip;
        $this->startTime = $startTime;
        $this->endTime = $endTime;
        $this->reason = $reason;
        $this->banner = $banner;
    }

    public static function getBy($arr){
        $a = array();
        foreach(Database::getRows('bans', $arr) as $row){
            $a[] = new Ban($row['Id'], $row['PlayerId'], $row['Ip'], $row['StartTime'], $row['EndTime'], $row['Reason'], $row['Banner']);
        }
        return $a;
    }

    public static function all($page = 0, $step = 25){
        $a = array();
        foreach(Database::getRows('bans', array(), '', $page*$step.''.$step) as $row){
            $a[] = new Ban($row['Id'], $row['PlayerId'], $row['Ip'], $row['StartTime'], $row['EndTime'], $row['Reason'], $row['Banner']);
        }
        return $a;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPlayerID()
    {
        return $this->playerID;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @return mixed
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @return mixed
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * @return mixed
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @return mixed
     */
    public function getBanner()
    {
        return $this->banner;
    }

}