<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 01/04/2019
 * Time: 12:39
 */

class Check
{
    public static function nan($var){
        return is_null($var) || $var !== false;
    }
    public static function num($var){
        if(!self::nan($var))return false;
        return preg_match('/^[0-9]+[\.|,][0-9]*$/',$var) === 1;
    }
    public static function mail($var){
        if(!self::nan($var))return false;
        return preg_match('/^[a-zA-Z0-9_\-\.]+@[a-zA-Z0-9-]+\.[a-zA-Z]{2,5}$/', $var) === 1;
    }
    public static function alpha($var){
        if(!self::nan($var))return false;
        return preg_match("/^[a-zA-Z0-9_\-]+$/", $var) === 1;
    }
    public static function ip($var){
        if(!self::nan($var))return false;
        return preg_match("/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/", $var) === 1;
    }
    public static function castTime($var){
        if(!self::nan($var)) return null;
        return Datetime::createFromFormat('Y-n-j H:i:s.u', $var);
    }

    /**
     * @param $var
     * @return string
     */
    public static function timeString($var){
        if(! $var instanceof DateTime) $var = Check::castTime($var);
        if($var == null) return "";
        return $var->format(Router::websiteValue('Y-n-j','display_date'));
    }

    public static function captcha($reCaptcha){
        $api_url = "https://www.google.com/recaptcha/api/siteverify?secret="
            .Router::websiteValue('', 'recaptcha_key_private')
            ."&response=".$reCaptcha
            ."&remoteip=".$_SERVER['REMOTE_ADDR'];
        $data = json_decode(file_get_contents($api_url), true);
        return $data;
    }
}