<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 30/03/2019
 * Time: 20:04
 */

class App
{
    private $var;
    private $alert;
    private $template;
    private static $instance;

    private function __construct()
    {
        $this->var = [];
        $this->alert = [];
        $this->template = "";
    }

    public static function asset($url){
        return Router::websiteValue('', 'path').'/'.$url;
    }

    public function setTemplate($path){
        App::$instance->template = 'template/'.$path.'.php';
    }

    public static function getInstance(){
        return App::$instance;
    }

    /**
     * @return string
     */
    public static function template(){
        return App::$instance->template;
    }

    public static function path($url){
        return substr($url, strlen(Router::websiteValue('/', 'path')));
    }

    public static function init(){
        App::$instance = new App();

        if(array_key_exists('server',$_SESSION))
            App::initDB();

        if(key_exists('routerAlert', $_SESSION))
            App::$instance->alert = json_decode($_SESSION['routerAlert'], true);

    }

    public static function initDB($index = null){
        if($index !== null)
            $server = Router::getConfigJSONRef(array(), 'servers', $index);
        else
            $server = Server::intersect();

        /** DATABASE PLAYER */
        $pl = $server['config_server']['PlayerDatabase'];
        $link = $pl['Server'];
        if(strtolower($pl['Type'])=='sqlite')
            $link = $server['intersect_folder_server']."/resources/playerdata.db";
        Database::addDB('PlayerDatabase',$link, $pl['Username'], $pl['Password'], $pl['Port'], $pl['Database'], $pl['Type']);

        /** DATABASE GAME */
        $ga = $server['config_server']['GameDatabase'];
        $link = $ga['Server'];
        if(strtolower($ga['Type'])=='sqlite')
            $link = $server['intersect_folder_server']."/resources/gamedata.db";
        Database::addDB('GameDatabase', $link, $ga['Username'], $ga['Password'], $ga['Port'], $ga['Database'], $ga['Type']);

        /** DATABASE WEB */
        if(!is_dir("var"))mkdir("var");
        if(!is_file("var/database.db"))new SQLite3('var/database.db');
        Database::addDB("WebDatabase", "var/database.db");

        /** SET DEFAULT NAME. NOT CHANGE ! */
        Database::setDefaultName('PlayerDatabase');
    }

    public static function bindFunc(){
        $class = Router::controller().'Controller';
        $str = Router::link();

        $class_methods = get_class_methods($class);
        foreach($class_methods as $class_method){
            if($str."Action" == $class_method){
                return call_user_func_array(array(new $class, $class_method), array());
            }
        }
        return null;
    }

    public static function addVar($key, $value){
        App::$instance->var[$key] = $value;
    }

    public static function getVar($key, $defaultValue =  null){
        if(key_exists($key, App::$instance->var)){
            return App::$instance->var[$key];
        }
        return $defaultValue;
    }

    /**
     * @param string $type
     * @param string $msg
     */
    public static function addAlert($type, $msg){
        App::$instance->alert[] = array($type => $msg);
        $_SESSION['routerAlert'] = json_encode(App::$instance->alert);
    }

    /**
     * @return array
     */
    public static function alert(){
        return App::$instance->alert;
    }

    public static function cleanAlert(){
        unset($_SESSION['routerAlert']);
    }

    public static function getCache($dir,$name){
        if(Router::websiteValue(false, 'debug'))return null;

        $filename = 'var/cache/'.$_SESSION['server'].'/'.$dir.hash('sha256', $name);
        if (is_file($filename)){
            if(filemtime($filename)+Router::websiteValue(7200, 'refresh_cache') > time())
                return json_decode(file_get_contents($filename), true);
            else
                return null;
        }
        return null;
    }

    public static function setCache($dir, $name, $row){
        if(Router::websiteValue(false, 'debug'))return;
        if($row == null || count($row) == 0) $row = array();


        $dir = 'var/cache/'.$_SESSION['server'].'/'.$dir;
        if(!is_dir($dir))
            mkdir($dir, 0777, true);

        $filename = $dir.hash('sha256', $name);
        try{
            $json = json_encode($row);
            if(file_exists($filename))
                unlink($filename);

            file_put_contents($filename, $json);
        }catch (Exception $ex){
            var_dump($ex);
        }
    }
}