# Version 0.9
- Add Event in backend
- Add shop and future encyclopedia (not implement yet)
- Add Jquery
- Remove json() in entities class
- Transform class Quest (implement Table)
- Add Controller method : indexAction for each file
- Add in class Table blob (type blob for sqlite)
- [Fix] installation GUI / CLI

# Version 0.8
- Add cache for request SQL
- Add cache for ranking
- Add in config.json durability cache (refresh each x times) => "refresh_cache" (default: 7200s)
- Variable in config.json set debug website or not
- Add composer and "Linfo" dependency. Need to get information hardware computer. (Option: activate in config.json)
- Create directory in template stock all template user
- Create language en and fr and implement in website
- Remove website name => only title
- Install with CLI (console) or GUI
    - 3 options:
        - Create a new server Intersect with Panel (Full Auto ;) )
        - Connect local server to panel (if you have installed server Intersect)
        - Connect remote server to panel (Need mysql for both database (Player and Game))
- Create class Server with reference to configuration server Intersect and Web
- Change method to create database and translate configuration to json (App.php)
- Add to chart with RAM and Proc to server
- [Fix] Show url if the function doesn't exist
- [Fix] API to status
- [Fix] Experience in character (not show)
- [Fix] Number in inventory
- [Fix] Quest link character (not list)
- [Fix] change check many information (Check.php)


# Version 0.7
- Edit config.json in web panel
- Add Copyright Intersect ;)
- Add show need time to generate page (in s)
- Add show number of sql query total for the page
- Bug Fix get value in config.json (Router)
- Simplification of the database layer
- Can add lot of database in App (initDB function) for ... I don't know :D
- Add Itable to create cohesion between tables in PHP
- Add WebServer to create news or other for panel. (Not use yet)
- Add modular ranking with 2 types (level, xp, variable, ...) => WARNING : System don't create a cache and need to calculate all request !
- Add documentation for config.json

# Version 0.6
- Add quests for each character
- Add list ban account in Administration Page
- Add npcs (only : Id and Name)
- Add list items in Administration Page
- Set api/login if need information or not
- Add recaptcha login (option in config.json)
- Can create subdirectory in directory class and controller
- Add Player variable. And Character have all variable with value
- Change color background and new color/design
- Add in Router, function to search many key in array
- Can edit value name and value inside variable with config.json

# Version 0.5
- Add in controller set access. If the visitor haven't access, he is redirect in index
- Show if item is equipped or not (in character)
- Add "Check::castTime" to show Date and time in template
- Add in Website => display_date (config.json) to edit how show datetime in website (using date() PHP)


- /!\ Bug API "/online" DB: ok / Server Intersect : Bug

# Version 0.4
- Administration : list all users and all maps with pages (previous and next)
- Create check  email/numeric/pseudo. Class Check
- Add possibility to get inventory for each character
- Router : Add null if config have not default game server
- Get Account in controller (getUser())
- API, if login successful, you can get all information in "account"
- Bug Fix UUID for sqlite in Database

# Version 0.3
- Change config.json (if specific, system take config.json in Intersect Server and load. Else get in config.json in this panel)
- Fix Database can have sqlite AND mysql in the same time (player and game data)
- Map is load with the name and id for each characters
- Alert move Router to App (logical)
- Create function in user to get all users (by step 25)
- Start panel administration with lists account and each account have all characters in table
- New API with online (check is service API, Mysql, Intersect Server are online or not). Link api/online
- Change User::Login to User::login
- In the top left website have name of actual server connected
- Simplification config.json
- And little modification

# Version 0.2
- API with login (account and password)
- Add possibility to add controllers
- Fix router
- Fix email and password changed
 
# Version 0.1
- Display information account
- Display characters for your account
- Edit password and email
- Display information fo each character
